let loader = (name) => {
	if (name == 'file') {
		$('#loader').text(`Menghapus ${name}.. `)
	} else {
		$('#loader').text(`Uploading ${name}.. `)
	}
}

hapus = (name, type, id = null) => {
	$('#error').hide();
	data = {};
	let nama = $(`#btn-${name}`).val();
	if (type == 'img') {
		data = {
			img: nama,
			id: id
		}
	} else if (type == 'pdf') {
		data = {
			pdf: nama,
			id: id
		}
	}
	loader('file');
	setTimeout(() => {
		$.ajax({
			method: 'post',
			url: './form_laporan/hapusFile',
			data: {
				file: data
			},
			success: (res) => {
				let del = JSON.parse(res);
				if (del.deleted) {
					$(`#${del.nama}`).remove();
				} else {
					$('#error').show();
					$('#error').addClass('alert alert-danger').text(`Ada kesalahan saat menghapus file`);
				}
			},
			error: (type, status) => {
				$('#error').show();
				$('#error').addClass('alert alert-danger').text(`Ada Kesalahan ! ${status}`);
			},
			complete: function () {
				$('#loader').text('');
			}
		})
	}, 2000);
}

$('#testPdf').on('change', () => {
	loader('pdf');
	$('#error').hide();
	let pdf = $('#testPdf').prop('files')[0];
	let pdfData = new FormData();
	pdfData.append('lampPdf', pdf);
	setTimeout(() => {
		$.ajax({
			method: 'post',
			url: './form_laporan/pdfUpload',
			data: pdfData,
			contentType: false,
			processData: false,
			success: function (pdf) {
				let pdfParse = JSON.parse(pdf);
				if (pdfParse.error) {
					$('#error').show();
					$('#testPdf').val('');
					$('#error').addClass('alert alert-danger').html(`${pdfParse.error}`)
				} else {
					$('#error').hide();
					$('#testPdf').val('');
					$('#streamPdf').append(`
						<br>
						<div class="col shad rounded p-2" id="${pdfParse.name}">
						<i class='fas fa-lg fa-file-pdf'></i>						
						<button type='button' onclick="hapus(${pdfParse.name},'pdf')" class='btn btn-sm btn-danger m-2' id='btn-${pdfParse.name}' value='${pdfParse.name}'>Hapus PDF</button>
						<input class="form-control form-control-sm" type='text' name='pdf[${pdfParse.name}]' placeholder="Nama lampiran"></input>
						</div>
						`)
				}
			},
			error: function (type, status) {
				$('#error').show();
				$('#error').addClass('alert alert-danger').text(`Ada Kesalahan ! ${status}`);
			},
			complete: function () {
				$('#loader').text('');
			}
		})
	}, 2000);
})

$('#testImage').on('change', () => {
	loader('image');
	$('#error').hide();
	let img = $('#testImage').prop('files')[0];
	let imgData = new FormData();
	imgData.append('lampFoto', img);
	setTimeout(() => {
		$.ajax({
			method: 'post',
			url: './form_laporan/imageUpload',
			data: imgData,
			contentType: false,
			processData: false,
			success: function (image) {
				let imgParse = JSON.parse(image);
				if (imgParse.error) {
					$('#error').show();
					$('#testImage').val('');
					$('#error').addClass('alert alert-danger').html(`${imgParse.error}`)
				} else {
					$('#error').hide();
					$('#testImage').val('');
					$('#streamImage').append(`
						<div class="col my-2 shad rounded" id="${imgParse.name}">
						<img src="${imgParse.image}" height="100px" width="100px" class="my-2 mx-2"></img>
						<button type='button' onclick="hapus(${imgParse.name},'img')" class='btn btn-sm btn-danger' id='btn-${imgParse.name}' value='${imgParse.name}'>Hapus Gambar</button>
						<input class="form-control form-control-sm mx-2 my-2" placeholder="Caption" type="text" name="caption[${imgParse.name}]"></input>
						</div>
						`)
				}
			},
			error: function (type, status) {
				$('#error').show();
				$('#error').addClass('alert alert-danger').text(`Ada Kesalahan ! ${status}`);
			},
			complete: function () {
				$('#loader').text('');
			}
		})
	}, 2000);
})
