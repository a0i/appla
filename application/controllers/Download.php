<?php

class Download extends CI_Controller
{
    public function index()
    {
        if ($_REQUEST['n'] != null) {
            $path = 'assets/fresh_pdf/' . $this->input->get('n');
            if (file_exists($path)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/pdf');
                header('Content-Disposition: attachment ; filename="' . basename($path) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($path));
                flush();
                readfile($path);
                exit;
            }
            // force_download('assets/fresh_pdf/' . $this->input->get('n'), null);
        }
    }
}
