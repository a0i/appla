<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ubah extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Laporan_model');
    }
    public function index()
    {
        $dok = decrypt_url($this->input->get('dok'));
        if ($dok != null) {
            $res = $this->Laporan_model->getLaporanById($dok);
            if ($res != false) {
                $data = [
                    'view' => 'user/laporan/update',
                    'dt' => $res
                ];
                $this->load->view('user/overview', $data);
            }
        }
    }

    public function update()
    {
        $decId = decrypt_url($this->input->post('id_dok'));
        $validasi = $this->form_validation;
        $validasi->set_rules($this->Laporan_model->rules());
        if ($validasi->run()) {
            $laporan = $this->Laporan_model->updateLaporan();
            if ($laporan != false) {
                $res = $this->Laporan_model->getLaporanById($laporan);
                $nm_file = $this->Laporan_model->cetak($res);
                if ($nm_file != false) {
                    redirect('ubah?dok=' . $this->input->post('id_dok') . '&fname=' . base64_encode($nm_file));
                    die;
                }
            } else {
                redirect('ubah?dok=' . $this->input->post('id_dok'));
                die;
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('ubah?dok=' . $this->input->post('id_dok'));
        }
    }

    // public function deleteLampiran()
    // {
    //     if ($this->input->get() != null) {
    //         $nm = base64_decode($this->input->get('nm'));
    //         if (file_exists('assets/img/gbr/' . $nm)) {
    //             $res = $this->Laporan_model->hpsGbr($nm, $this->input->get('id'));
    //             if (is_array($res)) {
    //                 if (isset($res['error'])) {
    //                     echo json_encode($res);
    //                     die;
    //                 };
    //                 echo json_encode(['gbr' => implode('?', $res)]);
    //                 die;
    //             }
    //         } else if (file_exists('assets/pdf/' . $nm)) {
    //             $res = $this->Laporan_model->hpsPdf($nm, $this->input->get('id'));
    //             if (isset($res['error'])) {
    //                 echo json_encode($res);
    //                 die;
    //             };
    //             echo json_encode(['pdf' => implode('?', $res)]);
    //             die;
    //         } else {
    //             echo json_encode(['error' => 'error']);
    //             die;
    //         }
    //     }
    //     echo json_encode(['error' => 'error']);
    //     die;
    // }
}
