<?php

// use setasign\Fpdi\Fpdi;

defined('BASEPATH') or exit('No direct script access allowed');

class Form_laporan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Laporan_model');
        $this->load->model('Dosen_model');
    }
    public function index()
    {
        $data = [
            'view' => 'user/laporan/index'
        ];
        $this->load->view('user/overview', $data);
    }

    public function imageUpload()
    {
        if ($_FILES['lampFoto']['error'] == 0) {
            $uploadData = $this->Laporan_model->imageUpload();
            if (!isset($uploadData['error'])) {
                $path = 'assets/img/gbr/' . $uploadData['file_name'];
                $name = explode('.', $uploadData['file_name']);
                $imgData = [
                    'image' => 'data: ' . mime_content_type($path) . ';base64,' . base64_encode(file_get_contents($path)),
                    'name' => $name[0]
                ];
            } else {
                $imgData = $uploadData;
            }

            echo json_encode($imgData);
        }
    }

    public function pdfUpload()
    {
        if ($_FILES['lampPdf']['error'] == 0) {
            $uploadData = $this->Laporan_model->pdfUpload();
            if (!isset($uploadData['error'])) {
                $name = explode('.', $uploadData['file_name']);
                $pdfData = [
                    'name' => $name[0]
                ];
            } else {
                $pdfData = $uploadData;
            }
            echo json_encode($pdfData);
        }
    }

    public function hapusFile()
    {
        $file = $this->input->post('file');
        if (isset($file['img'])) {
            $nama = $file['img'];
            if ($file['id'] != null) {
                $decId = decrypt_url($file['id']);
                $res = $this->Laporan_model->hpsGbr($decId);
                if ($res == false) {
                    $res = [
                        'deleted' => false,
                        'nama' => $nama
                    ];
                    die(json_encode($res));
                }
            }
            $imgPath = 'assets/img/gbr/' . $nama . '.png';
            if (file_exists($imgPath)) {
                $res = [
                    'deleted' => unlink($imgPath),
                    'nama' => $nama
                ];
            } else {
                $res = [
                    'deleted' => false,
                    'nama' => $nama
                ];
            }
        } else if (isset($file['pdf'])) {
            $nama = $file['pdf'];
            if ($file['id'] != null) {
                $decId = decrypt_url($file['id']);
                $res = $this->Laporan_model->hpsPdf($decId);
                if ($res == false) {
                    $res = [
                        'deleted' => false,
                        'nama' => $nama
                    ];
                    die(json_encode($res));
                }
            }
            $pdfPath = 'assets/pdf/' . $nama . '.pdf';
            if (file_exists($pdfPath)) {
                $res = [
                    'deleted' => unlink($pdfPath),
                    'nama' => $nama
                ];
            } else {
                $res = [
                    'deleted' => false,
                    'nama' => $nama
                ];
            }
        }

        die(json_encode($res));
    }

    public function add()
    {
        $validasi = $this->form_validation;
        $validasi->set_rules($this->Laporan_model->rules());
        $save = $this->input->post('save');
        if (isset($save)) {
            if ($validasi->run()) {
                $laporan = $this->Laporan_model->insertLaporan();
                if ($laporan != false) {
                    $res = $this->Laporan_model->getLaporanById($laporan);
                    $nm_file = $this->Laporan_model->cetak($res);
                    if ($nm_file != false) {
                        $data = [
                            'view' => 'user/laporan/index',
                            'nm_file' => $nm_file
                        ];
                        $this->load->view('user/overview', $data);
                    }
                } else {
                    $this->session->set_flashdata('error', 'Ada error pastikan file yang anda masukkan sesuai');
                    redirect('form_laporan');
                    die;
                }
            } else {
                $this->session->set_flashdata('error', validation_errors());
                $this->session->set_flashdata('last', $this->input->post());
                redirect('form_laporan');
                die;
            }
        }
    }

    // private function _cetak($data = null)
    // {
    //     $nm_file = $data->file_pdf;
    //     $nm_pdf = $data->lampiran_pdf;
    //     $isi['data'] = $data;
    //     try {
    //         ob_start();
    //         $this->load->view('cetak/overview', $isi);
    //         $html = ob_get_contents();
    //         require_once APPPATH . 'third_party/html2pdf.class.php';
    //         $pdf = new HTML2PDF('P', 'a4', 'en', true, 'UTF-8');
    //         $pdf->setDefaultFont('times', 12);
    //         $pdf->writeHTML($html);
    //         ob_end_clean();
    //         if ($_FILES['gambar']['error'][0] == 0 && $_FILES['pdf']['error'] == 0) {
    //             $pdf->Output('assets/pdf_old/' . $nm_file, 'F');
    //             $pdf_file = $this->_gabung(['assets/pdf_old/' . $nm_file, 'assets/pdf/' . $nm_pdf], $nm_file);
    //             return $pdf_file;
    //         } else if ($_FILES['pdf']['error'] == 0) {
    //             $pdf->Output('assets/pdf_old/' . $nm_file, 'F');
    //             $pdf_file = $this->_gabung(['assets/pdf_old/' . $nm_file, 'assets/pdf/' . $nm_pdf], $nm_file);
    //             return $pdf_file;
    //         } else {
    //             $pdf->Output('assets/fresh_pdf/' . $nm_file, 'F');
    //             return $nm_file;
    //         }
    //     } catch (Html2PdfException $e) {
    //         $pdf->clean();

    //         $formatter = new ExceptionFormatter($e);
    //         echo $formatter->getHtmlMessage();
    //     }
    // }

    // private function _gabung($files, $nm_file = null)
    // {
    //     require_once('vendor/setasign/fpdf/fpdf.php');
    //     require_once('vendor/setasign/fpdi/src/autoload.php');
    //     $fpdi = new Fpdi;
    //     foreach ($files as $file) {
    //         $pageCount = $fpdi->setSourceFile($file);
    //         for ($i = 1; $i <= $pageCount; $i++) {
    //             $pageId = $fpdi->importPage($i);
    //             $s = $fpdi->getTemplateSize($pageId);
    //             $fpdi->AddPage($s['orientation'], $s);
    //             $fpdi->useImportedPage($pageId);
    //         }
    //     }

    //     $fpdi->Output('assets/fresh_pdf/' . $nm_file, 'F');
    //     return $nm_file;
    // }
}
