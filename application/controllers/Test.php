<?php
use setasign\Fpdi\Fpdi;

defined('BASEPATH') or exit('No direct script access allowed');

class Test extends CI_Controller
{
    private $fpdi;

    public function index()
    {
        try {
            ob_start();
            $this->load->view('overview');
            $html = ob_get_contents();
            ob_get_clean();
            require_once APPPATH . 'third_party/html2pdf.class.php';
            $pdf = new HTML2PDF('P', 'a4', 'en', true, 'UTF-8');
            $pdf->setDefaultFont('times', 12);
            $pdf->writeHTML($html);
            $pdf->Output('assets/pdf_old/test.pdf', 'F');
            $this->gabung(['assets/pdf_old/test.pdf', 'assets/pedef.pdf']);
        } catch (Html2PdfException $e) {
            $pdf->clean();

            $formatter = new ExceptionFormatter($e);
            echo $formatter->getHtmlMessage();
        }
    }

    public function gabung($files)
    {
        require_once('vendor/setasign/fpdf/fpdf.php');
        require_once('vendor/setasign/fpdi/src/autoload.php');
        $this->fpdi = new Fpdi;

        foreach ($files as $file) {
            $pageCount = $this->fpdi->setSourceFile($file);
            for ($i = 1; $i <= $pageCount; $i++) {
                $pageId = $this->fpdi->importPage($i);
                $s = $this->fpdi->getTemplateSize($pageId);
                $this->fpdi->AddPage($s['orientation'], $s);
                $this->fpdi->useImportedPage($pageId);
            }
        }

        $this->fpdi->Output('okwok.pdf', 'I');
    }
}
