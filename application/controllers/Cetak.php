<?php
use setasign\Fpdi\Tfpdf\Fpdi;

defined('BASEPATH') or exit('No direct script access allowed');

class Cetak extends CI_Controller
{
    private $fpdi;
    private $fpdf;

    public function __construct()
    {
        require_once('vendor/setasign/fpdf/fpdf.php');
        require_once('vendor/setasign/fpdi/src/autoload.php');
        $this->fpdi = new Fpdi;
    }
}
