<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Laporan_model');
    }
    public function index()
    {

        $data['view'] = 'user/document/index.php';
        $data['laporan'] = $this->Laporan_model->getAllLaporan();
        $this->load->view('user/overview.php', $data);
    }

    public function data()
    {
        $id = $this->input->get('id');
        $decId = decrypt_url($id);
        if (decrypt_url($id) == false || $id == null) {
            return false;
        }
        $res = $this->Laporan_model->getLaporanById($decId);
        $lap = [
            'file_pdf' => $res->file_pdf,
            'id_dok' => encrypt_url($res->id_dok),
            'nama_dok' => $res->nama_dok
        ];
        die(json_encode($lap));
    }

    public function hapusLaporan()
    {
        $id = decrypt_url($this->input->get('id'));
        if ($id != null) {
            if ($this->Laporan_model->deleteLaporan($id)) {
                die('ok');
            }
            die('gk');
        }
    }
}
