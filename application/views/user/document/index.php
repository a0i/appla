<div class="row text-center">
    <?php if ($laporan) : ?>
        <?php foreach ($laporan as $key => $value) : ?>
            <div class="col-lg-3 col-md-4 col-sm-6 mt-2">
                <div class="card text-center" style="width: 15rem; border:none !important;">
                    <div class="card-body text-left p-2">
                        <a onclick="ubah('<?= encrypt_url($value->id_dok) ?>')" href="#" class="btn btn-primary doc"><i class="fa fa-1x fa-file"></i></a>
                        <a onclick="ubah('<?= encrypt_url($value->id_dok) ?>')" class="hrefing">
                            <h6 class="card-title d-inline text-wrap text-primary" class="hrefing"><?= $value->nama_dok ?></h6>
                        </a>
                    </div>
                    <a onclick="hapus('<?= encrypt_url($value->id_dok) ?>')" class="btn btn-sm btn-danger text-light p-0 ml-2">Hapus</a>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
<script>
    function hapus(params) {
        Swal.fire({
            title: 'Hapus Laporan',
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Jangan'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '<?= base_url('dashboard/hapusLaporan') ?>',
                    data: {
                        id: params
                    },
                    method: 'get',
                    success: function(res) {
                        if (res == 'ok') {
                            location.reload();
                        } else {
                            location.reload();
                        }
                    }
                });
            }
        });
    }

    function ubah(params) {
        $.ajax({
            method: 'get',
            url: '<?= base_url('dashboard/data') ?>',
            data: {
                id: params
            },
            success: function(params) {
                let data = JSON.parse(params);
                return aksi(data)
            }
        });
    }

    function aksi(params) {
        Swal.fire({
            title: '' + params.nama_dok,
            text: "Pilih aksi",
            type: 'question',
            showCancelButton: true,
            showCloseButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: 'green',
            confirmButtonText: 'Download',
            cancelButtonText: 'Ubah'
        }).then((result) => {
            if (result.value) {
                window.location.href = '<?= base_url('download?n=') ?>' + params.file_pdf;
                setInterval(function() {
                    window.location.href = '<?= base_url() ?>';
                }, 1000);
            } else if (result.dismiss == 'cancel') {
                window.location.href = '<?= base_url('ubah?dok=') ?>' + params.id_dok;
            }
        });
    }
</script>