<div class="box-in">
    <h1 class="text-center">FORM LAPORAN</h1>
    <?php if ($this->session->flashdata('error') != null) : ?>
    <div class="alert alert-danger"><?= $this->session->flashdata('error') ?></div>
    <?php endif; ?>
    <div id="error"></div>
    <?php $data = $this->session->flashdata('last') ?>
    <?= form_open_multipart('form_laporan/add') ?>
    <div class="panel panel-default">
        <div class="panel-heading"></div>
        <div class="panel-body">
            <div class="form-group">
                <label>Nama file <b class="text-danger">*</b></label>
                <div class="input-group text" id="judul">
                    <input type="text" class="w-100 m-1 p-1 input-bottom" name="dok" id="dok"
                        placeholder="Nama file" value="<?= $data['dok'] ?>" />
                </div>
            </div>
            <div class="form-group">
                <label>Judul Kegiatan <b class="text-danger">*</b></label>
                <div class="input-group text" id="judul">
                    <input type="text" class="w-100 m-1 p-1 input-bottom" name="judul" id="judul"
                        placeholder="Judul Kegiatan" value="<?= $data['judul'] ?>" />
                </div>
            </div>
            <div class="form-group">
                <label>Nama Laporan <b class="text-danger">*</b></label>
                <div class="input-group text" id="nama_lap">
                    <input type="text" class="w-100 m-1 p-1 input-bottom" name="nama_lap" id="nama_lap"
                        placeholder="Nama Laporan" value="<?= $data['nama_lap'] ?>" />
                </div>
            </div>
            <div class="form-group">
                <label>Tanggal Kegiatan <b class="text-danger">*</b></label>
                <div class="input-group date" id="tgl">
                    <input type="text" class="w-25 m-1 p-1 input-bottom text-center" name="tanggal" id="tanggal"
                        placeholder="Tanggal" value="<?= $data['tanggal'] ?>" />
                </div>
            </div>
            <div class="form-group">
                <label>Tempat <b class="text-danger">*</b></label>
                <div class="input-group text" id="tmpt">
                    <input type="text" class="w-100 m-1 p-1 input-bottom" name="tempat" id="tmpt"
                        placeholder="Tempat" value="<?= $data['tempat'] ?>" />
                </div>
            </div>
            <div class="form-group" id="tambah">
                <label>Pembicara/Fasilitator <b class="text-danger">*</b></label>
                <div class="input-group text">
                    <input type="text" class="w-100 m-1 p-1 input-bottom m-1" name="pembicara[]" placeholder="Pembicara"
                        value="<?= $data['pembicara'][0] ?>" />
                    <a class="btn btn-success text-light text-center btn-tambah" id="btn-tamb" onclick="actionp()"
                        title="Tambah Kolom Pembicara"><i class="fa fa-sm fa-plus"
                            style="color:white;position: relative;top: 4px;"></i></a>
                </div>
                <div id="adding0"></div>
            </div>
            <div class="form-group" id="fasil">
                <label>Fasilitas Peserta <b class="text-danger">*</b></label>
                <div class="input-group text">
                    <input type="text" class="w-100 m-1 p-1 input-bottom m-1" name="fasilitas[]"
                        placeholder="Fasilitas Peserta" value="<?= $data['fasilitas'][0] ?>" />
                    <a class="btn btn-success text-light text-center btn-tambah" id="btn-fas" onclick="actionf()"
                        title="Tambah Kolom Fasilitas"><i class="fa fa-sm fa-plus"
                            style="color:white;position: relative;top: 4px;"></i></a>
                </div>
                <div id="fadding0"></div>
            </div>
            <div class="form-group">
                <label>Peserta <b class="text-danger">*</b></label>
                <div class="input-group text" id="pesertaa">
                    <input type="text" class="w-100 m-1 p-1 input-bottom m-1" name="peserta" id="pesertaa"
                        placeholder="Tempat" value="<?= $data['peserta'] ?>" />
                </div>
            </div>
            <div class="form-group">
                <label>Materi Kegiatan <b class="text-danger">*</b></label>
                <div class="input-group text" id="materi">
                    <input type="text" class="w-100 m-1 p-1 input-bottom m-1" name="materi" id="materi"
                        placeholder="Materi Kegiatan" value="<?= $data['materi'] ?>" />
                </div>
            </div>
            <div class="form-group">
                <label>Tanggal Laporan <b class="text-danger">*</b></label>
                <div class="input-group date">
                    <div class="row">
                        <div class="col-sm-3">
                            <input type="text" class="w-100 m-1 p-1 input-bottom m-1 text-center" name="lokasi"
                                placeholder="Lokasi" value="<?= $data['lokasi'] ?>" />
                        </div>
                        <div class="col-sm-9">
                            <input type="text" class="w-75 m-1 p-1 input-bottom m-1 text-center" name="tgl_laporan"
                                placeholder="Tanggal Laporan" value="<?= $data['tgl_laporan'] ?>" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Mengetahui <b class="text-danger">*</b></label>
                <div class="input-group date">
                    <div class="col-sm-4">
                        <input type="text" class="w-100 m-1 p-1 input-bottom m-1" name="nama_kap" placeholder="Nama"
                            value="<?= $data['nama_kap'] ?>" />
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="w-100 m-1 p-1 input-bottom m-1 text-center" name="nipy_kap"
                            placeholder="NIPY/NIP/NIDK" value="<?= $data['nipy_kap'] ?>" />
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Penyusun Laporan <b class="text-danger">*</b></label>
                <br>
                <span>
                    <div class="input-group date">
                        <div class="col-sm-12">
                            <label>Jumlah Penyusun</label>
                            <select name="jumlahpenyusun" id="jumlahpenyusun" onchange="changepenyusun()">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                        </div>
                    </div>
                </span>
                <div id="penyusun">
                    <div class="input-group date">
                        <div class="col-sm-4">
                            <input type="text" class="w-100 m-1 p-1 input-bottom m-1" name="nama" placeholder="Nama"
                                value="<?= $data['nama'] ?>" />
                        </div>
                        <div class="col-sm-4">
                            <input type="text" class="w-100 m-1 p-1 input-bottom m-1 text-center" name="nipy"
                                placeholder="NIPY/NIP/NIDK" value="<?= $data['nipy'] ?>" />
                        </div>
                    </div>
                </div>
            </div>



            <!-- <div class="form-group">
                <div class="row" style="margin: 0 auto">
                    <div class="col-xs-12 text-right">
                        <p class="li-inline-b" style="margin-left:4px;">Lampirkan</p>
                        <li class="li-inline-b">
                            <div class="btn-tomb">
                                <input type="file" name="pdf" id="pd">
                                <label for="pd"><i class="fa fa-2x fa-file-pdf" style="color:white;padding-top: 10px;margin: 0px;"></i></label>
                            </div>
                        </li>
                        <li class="li-inline-b">
                            <div class="btn-tomb">
                                <input type="file" name="gambar[]" id="pic" multiple>
                                <label for="pic">
                                    <i class="fa fa-2x fa-camera" style="color:white;padding-top: 10px;margin: 0px;"></i>
                                </label>
                            </div>
                        </li>
                    </div>
                </div>
            </div> -->
            <div id="loader" class="mb-2"></div>
            <div class="row">
                <div class="col">
                    <div class="row">
                        <div class="col-sm-2">
                            <label for="testImage">
                                <i class="fa fa-lg fa-camera"></i>
                                <div class="d-inline-block">
                                    Gambar
                                </div>
                            </label>
                        </div>
                        <div class="col-sm-8">
                            <input type="file" name="testImage" id="testImage" required>
                            <div id="streamImage"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col" id="pdfForm">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="testPdf">
                                <i class="fa fa-lg fa-file-pdf"></i>
                                <div class="text-center d-inline-block ml-1">
                                    Pdf
                                </div>
                            </label>
                        </div>
                        <div class="col-sm-8">
                            <input type="file" name="testPdf" id="testPdf">
                            <div id="streamPdf"></div>
                        </div>
                    </div>
                </div>
                <div class="col">

                </div>
            </div>
            <div class="row mt-3" style="margin: 0 auto">
                <div class="col-xs-6">
                    <button type="submit" name="save" class="btn btn-primary btn-sm "
                        style="padding: 6px 8px;box-shadow: 1px 2px 2px gray;" title="Menyimpan menjadi PDF">Save to
                        PDF</button>
                </div>
            </div>
        </div>
    </div>
    <?= form_close() ?>
</div>
<script src="<?= base_url() ?>assets/js/form-tambah.js">
</script>
<script>

if ('<?= @$nm_file ?>' != '') {
    Swal.fire({
        title: 'File sudah terbuat',
        text: "Silahkan klik download untuk mendownloadnya",
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Download'
    }).then((result) => {
        if (result.value) {
            window.location.href = '<?= base_url('
            download ? n = ') . @$nm_file ?>';
            setInterval(function() {
                window.location.href = '<?= base_url('
                form_laporan ') ?>';
            }, 1000);
        } else {
            window.location.href = '<?= base_url('
            form_laporan ') ?>';
        }
    })
}
</script>