<?php if (@$dt) : ?>
    <div class="box-in">
        <h1 class="text-center">FORM UBAH LAPORAN</h1>
        <?php if ($this->session->flashdata('error') != null) : ?>
            <div class="alert alert-danger"><?= $this->session->flashdata('error') ?></div>
        <?php endif; ?>
        <?= form_open_multipart('ubah/update') ?>
        <?php $id = encrypt_url($dt->id_dok) ?>
        <input type='hidden' value='<?= $id ?>' id="id_dok" name='id_dok'>
        <input type="hidden" name="file_pdf" value="<?= base64_encode($dt->file_pdf) ?>">
        <div class="panel panel-default">
            <div class="panel-heading"></div>
            <div class="panel-body">
                <div class="form-group">
                    <label>Nama File</label>
                    <?php $nm_dok = explode('_', $dt->nama_dok) ?>
                    <div class="input-group text" id="judul">
                        <input type="text" class="w-100 m-1 p-1 input-bottom" name="dok" id="dok" placeholder="Nama File" value="<?= $nm_dok[0] ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label>Nama Laporan</label>
                    <div class="input-group text" id="nama_lap">
                        <input type="text" class="w-100 m-1 p-1 input-bottom" name="nama_lap" id="nama_lap" placeholder="Nama Laporan" value="<?= $dt->nama_lap ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label>Judul Kegiatan</label>
                    <div class="input-group text" id="judul">
                        <input type="text" class="w-100 m-1 p-1 input-bottom" name="judul" id="judul" placeholder="Judul Kegiatan" value="<?= $dt->judul ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label>Tanggal Kegiatan</label>
                    <div class="input-group date" id="tgl">
                        <input type="text" class="w-100 m-1 p-1 input-bottom" name="tanggal" id="tanggal" placeholder="Tanggal" value="<?= $dt->tanggal ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label>Tempat</label>
                    <div class="input-group text" id="tmpt">
                        <input type="text" class="w-100 m-1 p-1 input-bottom" name="tempat" id="tmpt" placeholder="Tempat" value="<?= $dt->tempat ?>" />
                    </div>
                </div>
                <div class="form-group" id="tambah">
                    <label>Pembicara/Fasilitator</label>
                    <?php $pemb = explode('?', rtrim($dt->pembicara, '? ')) ?>
                    <?php $c = count($pemb); ?>
                    <?php $con = 0 ?>
                    <?php foreach ($pemb as $key => $value) : ?>
                        <?php if ($c >= 1) : ?>
                            <?php if ($con == 0) : ?>
                                <div class="input-group text">
                                    <input type="text" class="w-100 m-1 p-1 input-bottom" name="pembicara[]" placeholder="Pembicara" value="<?= array_shift($pemb) ?>" />
                                    <a class="btn btn-success text-light text-center btn-tambah" id="btn-tamb" onclick="actionp()" title="Tambah Kolom Pembicara"><i class="fa fa-sm fa-plus" style="color:white;position: relative;top: 4px;"></i></a>
                                </div>
                                <?php $con++; ?>
                                <?php continue; ?>
                            <?php endif; ?>
                            <div class="input-group text">
                                <input type="text" class="w-100 m-1 p-1 input-bottom" name="pembicara[]" placeholder="Pembicara" value="<?= $value ?>" />
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <div id="adding0"></div>
                </div>
                <div class="form-group" id="fasil">
                    <label>Fasilitas Peserta</label>
                    <?php $fass = explode('?', rtrim($dt->fasilitas, '? ')) ?>
                    <?php $co = count($fass); ?>
                    <?php $conn = 0 ?>
                    <?php foreach ($fass as $key => $value) : ?>
                        <?php if ($co >= 1) : ?>
                            <?php if ($conn == 0) : ?>
                                <div class="input-group text">
                                    <input type="text" class="w-100 m-1 p-1 input-bottom" name="fasilitas[]" placeholder="Fasilitas Peserta" value="<?= array_shift($fass) ?>" />
                                    <a class="btn btn-success text-light text-center btn-tambah" id="btn-fas" onclick="actionf()" title="Tambah Kolom Fasilitas"><i class="fa fa-sm fa-plus" style="color:white;position: relative;top: 4px;"></i></a>
                                </div>
                                <?php $conn++; ?>
                                <?php continue; ?>
                            <?php endif; ?>
                            <div class="input-group text">
                                <input type="text" class="w-100 m-1 p-1 input-bottom" name="fasilitas[]" placeholder="Fasilitas Peserta" value="<?= $value ?>" />
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <div id="fadding0"></div>
                </div>
                <div class="form-group">
                    <label>Peserta</label>
                    <div class="input-group text" id="pesertaa">
                        <input type="text" class="w-100 m-1 p-1 input-bottom" name="peserta" id="pesertaa" placeholder="Tempat" value="<?= $dt->peserta ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label>Materi Kegiatan</label>
                    <div class="input-group text" id="materi">
                        <input type="text" class="w-100 m-1 p-1 input-bottom" name="materi" id="materi" placeholder="Materi Kegiatan" value="<?= $dt->materi ?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label>Tanggal Laporan</label>
                    <?php $tmptl = explode(',', $dt->tmp_tgl) ?>
                    <div class="input-group date">
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" class="w-100 m-1 p-1 input-bottom text-center" name="lokasi" placeholder="Lokasi" value="<?= $tmptl[0] ?>" />
                            </div>
                            <div class="col-sm-9">
                                <input type="text" class="w-75 m-1 p-1 input-bottom m-1 text-center" name="tgl_laporan" placeholder="Tanggal Laporan" value="<?= trim($tmptl[1], ' ') ?>" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Mengetahui</label>
                    <?php $kapr = explode('?', $dt->kaprodi) ?>
                    <div class="input-group date">
                        <div class="col-sm-4">
                            <input type="text" class="w-100 m-1 p-1 input-bottom" name="nama_kap" placeholder="Nama" value="<?= $kapr[0] ?>" />
                        </div>
                        <div class="col-sm-4">
                            <input type="text" class="w-100 m-1 p-1 input-bottom text-center" name="nipy_kap" placeholder="NIPY/NIP/NIDK" value="<?= $kapr[1] ?>" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Pembuat Laporan</label>
                    <?php $pm = explode('?', $dt->pembuat) ?>
                    <div class="input-group date">
                        <div class="col-sm-4">
                            <input type="text" class="w-100 m-1 p-1 input-bottom" name="nama" placeholder="Nama" value="<?= $pm[0] ?>" />
                        </div>
                        <div class="col-sm-4">
                            <input type="text" class="w-100 m-1 p-1 input-bottom text-center" name="nipy" placeholder="NIPY/NIP/NIDK" value="<?= $pm[1] ?>" />
                        </div>
                    </div>
                </div>



                <!-- <div class="form-group">
																																																															<div class="row" style="margin: 0 auto">
																																																																<div class="col-xs-12 text-right">
																																																																	<p class="li-inline-b" style="margin-left:4px;">Lampirkan</p>
																																																																	<li class="li-inline-b">
																																																																		<div class="btn-tomb">
																																																																			<input type="file" name="pdf" id="pd">
																																																																			<label for="pd"><i class="fa fa-2x fa-file-pdf" style="color:white;padding-top: 10px;margin: 0px;"></i></label>
																																																																		</div>
																																																																	</li>
																																																																	<li class="li-inline-b">
																																																																		<div class="btn-tomb">
																																																																			<input type="file" name="gambar[]" id="pic" multiple>
																																																																			<label for="pic">
																																																																				<i class="fa fa-2x fa-camera" style="color:white;padding-top: 10px;margin: 0px;"></i>
																																																																			</label>
																																																																		</div>
																																																																	</li>
																																																																</div>
																																																															</div>
																																																														</div> -->
                <div id="loader" class="mb-2"></div>
                <div class="row">
                    <div class="col">
                        <div class="row">
                            <div class="col-sm-2">
                                <label for="testImage">
                                    <i class="fa fa-lg fa-camera"></i>
                                    <div class="d-inline-block">
                                        Gambar
                                    </div>
                                </label>
                            </div>
                            <div class="col-sm-8">
                                <input type="file" name="testImage" id="testImage">
                                <div id="streamImage">
                                    <?php if (isset($dt->files)) : ?>
                                        <?php foreach ($dt->files as $value) : ?>
                                            <?php if ($value->jenis == 'gambar') : ?>
                                                <?php $gambar = explode('.', $value->file) ?>
                                                <div class="col my-2 shad rounded p-1 pr-3" id="<?= $gambar[0] ?>">
                                                    <img src="assets/img/gbr/<?= $value->file ?>" height="100px" width="100px" class="my-2 mx-2"></img>
                                                    <button type='button' onclick="hapus(<?= $gambar[0] ?>,'img','<?= encrypt_url($value->id_lampiran) ?>')" class='btn btn-sm btn-danger' id='btn-<?= $gambar[0] ?>' value='<?= $gambar[0] ?>'>Hapus</button>
                                                    <input type="text" placeholder="Caption" name="ada[<?= encrypt_url($value->id_lampiran) ?>]" value="<?= $value->caption ?>" class="form-control form-control-sm mx-2 my-2"></input>
                                                </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col" id="pdfForm">
                    <div class="row">
                        <div class="col-sm-2">
                            <label for="testPdf">
                                <i class="fa fa-lg fa-file-pdf"></i>
                                <div class="text-center d-inline-block ml-1">
                                    Pdf
                                </div>
                            </label>
                        </div>
                        <div class="col-sm-8">
                            <input type="file" name="testPdf" id="testPdf">
                            <div id="streamPdf">
                                <?php if (isset($dt->files)) : ?>
                                    <?php foreach ($dt->files as $value) : ?>
                                        <?php if ($value->jenis == 'pdf') : ?>
                                            <?php $pdf = explode('.', $value->file) ?>
                                            <div class="col shad rounded p-2" id="<?= $pdf[0] ?>">
                                                <i class='fas fa-lg fa-file-pdf'></i>
                                                <button type='button' onclick="hapus(<?= $pdf[0] ?>,'pdf','<?= encrypt_url($value->id_lampiran) ?>')" class='btn btn-sm btn-danger m-2' id='btn-<?= $pdf[0] ?>' value='<?= $pdf[0] ?>'>Hapus</button>
                                                <input type='text' placeholder="Nama lampiran" class="form-control form-control-sm" value='<?= $value->caption ?>' name='ada[<?= encrypt_url($value->id_lampiran) ?>]'></input>
                                            </div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-3" style="margin: 0 auto">
                <div class="col-xs-6">
                    <button type="submit" name="save" class="btn btn-primary btn-sm" style="padding: 6px 8px;box-shadow: 1px 2px 2px gray;" title="Menyimpan menjadi PDF">Save to
                        PDF</button>
                </div>
            </div>
            <?= form_close() ?>
        </div>
        <script src="<?= base_url('assets/js/form-tambah.js') ?>"></script>
        <?php $nm_file = ($this->input->get('fname') == null) ? false : base64_decode($this->input->get('fname')); ?>
        <script>
            if ('<?= @$nm_file ?>' != false) {
                Swal.fire({
                    title: 'File sudah ter update',
                    text: "Silahkan klik download untuk mendownloadnya",
                    type: 'success',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Download'
                }).then((result) => {
                    if (result.value) {
                        window.location.href = '<?= base_url('download?n=') . @$nm_file; ?>';
                        setInterval(function() {
                            window.location.href = '<?= base_url('ubah?dok=') . encrypt_url($dt->id_dok) ?>';
                        }, 1000);
                    } else {
                        window.location.href = '<?= base_url('
                ubah?dok=') . encrypt_url($dt->id_dok) ?>';
                    }
                })
            }

            // $('[id="hps"]').on('click', function(params) {
            // 	params.preventDefault();
            // 	const nama = $(this).attr('value');
            // 	const id = $('#id_dok').attr('value');
            // 	Swal.fire({
            // 		type: 'question',
            // 		text: 'Hapus file',
            // 		showCan
            celButton: true,
            // 		confirmButtonText: 'Hapus'
            // 	}).then((result) => {
            // 		if (result.value) {
            // 			$.ajax({
            // 				url: '<?= base_url('ubah/deleteLampiran') ?>',
            // 				data: {
            // 					nm: nama,
            // 					id: id
            // 				},
            // 				method: 'get',
            // 				success: (result) => {
            // 					let gmbr = JSON.parse(result).gbr;
            // 					let pdef = JSON.parse(result).pdf;
            // 					console.log(JSON.parse(result));
            // 					if (JSON.parse(result).error) {
            // 						Swal.fire({
            // 							type: 'error',
            // 							text: 'Ada error !',
            // 							showConfirmButton: false
            // 						});
            // 						setInterval(function() {
            // 							location.reload();
            // 						}, 1500);
            // 					} else if (gmbr || gmbr == '') {
            // 						$('#lamp_gbr').val(gmbr)
            // 						$(this).replaceWith('<span class="badge badge-info">Terhapus</span>');
            // 					} else if (pdef == "") {
            // 						$('#lamp_pdf').val(pdef)
            // 						$(this).replaceWith('<span class="badge badge-info">Terhapus</span>');
            // 					}
            // 				}
            // 			});
            // 		}
            // 	});

            // })
        </script>
    <?php endif; ?>