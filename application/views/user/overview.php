<!doctype html>
<html>

<?php $this->load->view('user/_partials/head.php') ?>

<body>
    <?php $this->load->view('user/_partials/nav.php') ?>
    <div class="container-fluid">
        <?php $this->load->view(@$view) ?>
    </div>
</body>

</html>