<?php $kp = explode('?', $data->kaprodi) ?>
<?php $kt = explode('?', $data->pembuat) ?>
<page backtop="40mm" backbottom="30mm" backleft="40mm" backright="30mm"
    style="font-size: 14px;font-family: 'Times New Roman', Times, serif">
    <table align="center" style="text-align: center;">
        <tr>
            <td style="height: 20mm"><b><?= strtoupper($data->judul) ?></b></td>
        </tr>
    </table>
    <table align="center" style="text-align: center">
        <tr>
            <td>
                <img src="<?= base_url('assets/img/logo.png') ?>" alt="" width=225 height=230>
            </td>
        </tr>
    </table>
    <table align="center" style="text-align: center;">
        <tr>
            <td style="height: 20mm;padding-top:10px"><b><?= $data->nama_lap ?></b></td>
        </tr>
    </table>
    <table align="center" style="text-align: center;padding-top:100px">
        <tr>
            <td>Disusun oleh</td>
        </tr>
        <tr>
            <td><?= $kt[0] ?></td>
        </tr>
    </table>
    <table align="center" style="text-align: center;padding-top: 200px">
        <tr>
            <td>POLITEKNIK HARAPAN BERSAMA TEGAL</td>
        </tr>
        <tr>
            <td><?= date('Y') ?></td>
        </tr>
    </table>
    <!-- <page_footer backbottom="30mm">
        <table class="page_footer" align="center" backleft="40mm">
            <tr>
                <td style="text-align: center;">
                    <?php $no = 1 ?>
                    <?= $no++ ?>
                </td>
            </tr>
        </table>
    </page_footer> -->
</page>
<page backtop="40mm" backbottom="30mm" backleft="40mm" backright="30mm"
    style="font-size: 12px;font-family: 'Times New Roman', Times, serif">
    <div>
        <ul style="list-style-type: none;">
            <li>
                <b>A. Judul Kegiatan<span style="margin-left: 15mm;">:</span> </b>
                <!-- <p style="padding-left: 15px;word-wrap: break-word; width: 50%">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates, dolorum. Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe, aspernatur. Consequatur amet atque at modi ducimus eveniet quidem provident perspiciatis!
            </p> -->
            </li>
            <ol style="list-style-type: none;">
                <li style="word-wrap: break-word; width:80%;text-align: justify;">
                    <div style="margin-left: -8mm;display: inline-block;"><?= $data->judul; ?></div>
                </li>
            </ol>
        </ul>
    </div>
    <div>
        <ul style="list-style-type: none;">
            <li>
                <b>B. Tanggal<span style="margin-left: 25mm;">:</span></b>
            </li>
            <ol style="list-style-type: none">
                <li style="word-wrap: break-word; width:80%;text-align: justify;padding-left: 12.4px;">
                    <div style="margin-left: -17mm;display: inline-block;"><?= $data->tanggal ?></div>
                </li>
            </ol>
        </ul>
    </div>
    <diV>
        <ul style="list-style-type: none;">
            <li>
                <b>C. Tempat <span style="margin-left: 24.5mm;">:</span></b>
            </li>
            <ol style="list-style-type: none">
                <li style="word-wrap: break-word;width:80%;text-align: justify;padding-left: 12.5px;">
                    <div style="margin-left: -17mm;display: inline-block;"><?= $data->tempat ?></div>
                </li>
            </ol>
        </ul>
    </div>
    <div>
        <ul style="list-style-type: none;">
            <li>
                <b>D. Pembicara / Fasilitator <span style="margin-left: 3.8mm;">:</span></b>
            </li>
            <?php $pem = 1 ?>
            <ol style="margin-left:-25px;">
                <?php foreach (explode('?', rtrim($data->pembicara, '? ')) as $key => $value) : ?>
                <li style="word-wrap: break-word;">
                    <?= $pem ?>. <?= $value ?>
                </li>
                <?php $pem++; ?>
                <?php endforeach; ?>
            </ol>
        </ul>
    </div>
    <div>
        <ul style="list-style-type: none;">
            <li>
                <b>E. Fasilitas Peserta <span style="margin-left: 13mm;">:</span></b>
            </li>
            <?php $fas = 1 ?>
            <ol style="margin-left:-25px;">
                <?php foreach (explode('?', rtrim($data->fasilitas, '? ')) as $key => $value) : ?>
                <li style="word-wrap: break-word;">
                    <?= $fas ?>. <?= $value ?>
                </li>
                <?php $fas++; ?>
                <?php endforeach; ?>
            </ol>
        </ul>
    </div>
    <div>
        <ul style="list-style-type: none;">
            <li>
                <b>F. Peserta <span style="margin-left: 25.2mm;">:</span></b>
            </li>
            <ol style="list-style-type: none;margin-left:-25px;">
                <li style="word-wrap: break-word;width: 65%;">
                    <?= $data->peserta ?>
                </li>
            </ol>
        </ul>
    </div>
    <div style="padding-bottom: 100px;">
        <ul style="list-style-type: none;">
            <li>
                <b>G. Materi Kegiatan <span style="margin-left: 12.3mm;">:</span></b>
            </li>
            <ol style="list-style-type: none;">
                <li style="word-wrap: break-word; width:62%;text-align: justify;">
                    <div style="margin-left: -6mm;"><i><?= $data->materi ?></i></div>
                </li>
            </ol>
        </ul>
    </div>
    <div style="padding-left: 50px">
        <table style="width: 70%;">
            <tr>
                <td style="text-align: left;    width: 60%"></td>
                <td style="text-align: left;    width: 40%"><?= $data->tmp_tgl ?></td>
            </tr>
            <tr>
                <td style="text-align: left;    width: 60%">Mengetahui,</td>
                <td style="text-align: left;    width: 40%"></td>
            </tr>
            <tr>
                <td style="text-align: left;    width: 60%">Ka. Prodi Akuntansi</td>
                <td style="text-align: left;    width: 40%"></td>
            </tr>
            
            <tr>
                <td style="text-align: left;    width: 60% ;padding: 40px 0px;"></td>
                <td style="text-align: left;    width: 40% ; padding: 40px 0px;"></td>
            </tr>
            <tr>
                <td style="text-align: left;    width: 60%"><?= $kp[0]; ?></td>
                <td style="text-align: left;    width: 40%"><?= $kt[0]; ?></td>
            </tr>
            <tr>
                <td style="text-align: left;    width: 60%">NIPY: <?= $kp[1]; ?></td>
                <td style="text-align: left;    width: 40%">NIPY: <?= $kt[1]; ?></td>
            </tr>
        </table>
    </div>

<!--
    <div style="padding-left: 50px">
        <table style="width: 70%;">
            <tr>
                <td style="text-align: left;    width: 60%"></td>
                <td style="text-align: left;    width: 40%"><?= $data->tmp_tgl ?></td>
            </tr>
            <tr>
                <td style="text-align: left;    width: 60%">Penyusun</td>
                <td style="text-align: left;    width: 40%">Penyusun</td>
            </tr>
            
            <tr>
                <td style="text-align: left;    width: 60% ;padding: 40px 0px;"></td>
                <td style="text-align: left;    width: 40% ; padding: 40px 0px;"></td>
            </tr>
            <tr>
                <td style="text-align: left;    width: 60%"><?= $kp[0]; ?></td>
                <td style="text-align: left;    width: 40%"><?= $kt[0]; ?></td>
            </tr>
            <tr>
                <td style="text-align: left;    width: 60%">NIPY: <?= $kp[1]; ?></td>
                <td style="text-align: left;    width: 40%">NIPY: <?= $kt[1]; ?></td>
            </tr>
            <tr>
                <td style="text-align: left;    width: 60% ;padding: 10px 0px;"></td>
                <td style="text-align: left;    width: 40% ; padding: 10px 0px;"></td>
            </tr>

            <tr>
                <td style="text-align: left;    width: 60%;padding-left: 150px;">Mengetahui,</td>
            </tr>
            <tr>
                <td style="text-align: left;    width: 60%;padding-left: 150px;">Ka. Prodi Akuntansi</td>
            </tr>
            
            <tr>
                <td style="text-align: left;    width: 60% ;padding:30px 0px;"></td>
            </tr>
            <tr>
                <td style="text-align: left;    width: 50%;padding-left: 150px;"><?= $kp[0]; ?></td>
            </tr>
            <tr>
                <td style="text-align: left;    width: 60%;padding-left: 150px;">NIPY: <?= $kp[1]; ?></td>
            </tr>
        </table>
    </div>
-->

<!--     <div style="padding-left: 50px">
        <table style="width: 70%;">
            <tr>
                <td style="text-align: left;    width: 60%"></td>
                <td style="text-align: left;    width: 40%"><?= $data->tmp_tgl ?></td>
            </tr>
            <tr>
                <td style="text-align: left;    width: 60%">Penyusun</td>
                <td style="text-align: left;    width: 40%">Penyusun</td>
            </tr>

            <tr>
                <td style="text-align: left;    width: 60% ;padding: 40px 0px;"></td>
                <td style="text-align: left;    width: 40% ; padding: 40px 0px;"></td>
            </tr>

            <tr>
                <td style="text-align: left;    width: 60%"><?= $kp[0]; ?></td>
                <td style="text-align: left;    width: 40%"><?= $kt[0]; ?></td>
            </tr>
            <tr>
                <td style="text-align: left;    width: 60%">NIPY: <?= $kp[1]; ?></td>
                <td style="text-align: left;    width: 40%">NIPY: <?= $kt[1]; ?></td>
            </tr>
			<tr>
                <td style="text-align: left;    width: 60% ;padding: 10px 0px;"></td>
                <td style="text-align: left;    width: 40% ; padding: 10px 0px;"></td>
            </tr>
            <tr>
                <td style="text-align: left;    width: 60%">Mengetahui,</td>
                <td style="text-align: left;    width: 40%"></td>
            </tr>
            <tr>
                <td style="text-align: left;    width: 60%">Ka. Prodi Akuntansi</td>
                <td style="text-align: left;    width: 40%">Penyusun</td>
            </tr>

            <tr>
                <td style="text-align: left;    width: 60% ;padding: 40px 0px;"></td>
                <td style="text-align: left;    width: 40% ; padding: 40px 0px;"></td>
            </tr>
            <tr>
                <td style="text-align: left;    width: 60%"><?= $kp[0]; ?></td>
                <td style="text-align: left;    width: 40%"><?= $kt[0]; ?></td>
            </tr>
            <tr>
                <td style="text-align: left;    width: 60%">NIPY: <?= $kp[1]; ?></td>
                <td style="text-align: left;    width: 40%">NIPY: <?= $kt[1]; ?></td>
            </tr>
        </table>
    </div> -->

    <!-- <ul style="list-style-type: none;">
        <li>
            <b>F. Peserta</b>
        </li>
        <ol style="list-style-type: none">
            <li style="word-wrap: break-word; width:90%;text-align: justify;padding-left: 12.5px;">
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Nisi animi quam odit atque tempore a. Consequuntur excepturi similique, consectetur voluptates nam sequi. Rerum amet, vitae accusamus quia quaerat accusantium odio.
            </li>
        </ol>
    </ul> -->
    <!-- <page_footer backbottom="30mm">
        <table class="page_footer" align="center">
            <tr>
                <td style="text-align: center">
                    <?= $no++ ?>
                </td>
            </tr>
        </table>
    </page_footer> -->
</page>
<?php if ($data->files != null) : ?>
<page backtop="40mm" backbottom="30mm" backleft="40mm" backright="30mm" style="font-size: 60px;">
    <h1 style="position: relative;top:0;bottom:500;font-size: 60px;text-align:center;">LAMPIRAN</h1>
</page>
<?php foreach ($data->files as $value) {
        if ($value->jenis == 'gambar') {
            $files = 'gambar';
            break;
        } else {
            $files = null;
        }
    } ?>
<?php if ($files == 'gambar') : ?>
<page backtop="40mm" backbottom="30mm" backleft="40mm" backright="30mm" style="font-size: 12px;">
    <table align="center" style="text-align: center;">
        <tr>
            <td style="font-size:14;"><b>DOKUMENTASI</b></td>
        </tr>
    </table>
    <table>
        <?php foreach ($data->files as $value) :  ?>
        <tr>
            <td>
                <?php if ($value->jenis == 'gambar') : ?>
                <img src="<?= base_url('assets/img/gbr/') . $value->file ?>" align="center"
                    style="display: block;width: 450px;margin: 0px auto;">
                <table align="center" style="text-align: center;">
                    <tr>
                        <td style="height: 1mm"><b></b></td>
                    </tr>
                </table>
                <table border="1px" style="border-collapse:collapse;width:430px;">
                    <tr style="width:430px;">
                        <td style="width:430px;padding:4px;">
                            <span style="display: block;"><?= $value->caption ?></span>
                        </td>
                    </tr>
                </table>
                <?php endif; ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
</page>
<?php endif; ?>
<?php endif; ?>
