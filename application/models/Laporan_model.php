<?php

use setasign\Fpdi\Fpdi;

class Laporan_model extends CI_Model
{
    private $_tbDoc = 'dokumen';
    private $_tbDDoc = 'detail_dok';
    private $_tbDos = 'dosen';
    public $nama_lap;
    public $id_dok;
    public $judul;
    public $tanggal;
    public $tempat;
    public $pembicara;
    public $fasilitas;
    public $peserta;
    public $materi;
    public $pembuat;
    public $kaprodi;
    public $tmp_tgl;
    public $file_pdf;

    public function rules()
    {
        return [
            [
                'label' => 'Nama Laporan',
                'field' => 'nama_lap',
                'rules' => 'required'
            ],
            [
                "label" => "Nama file",
                "field" => "dok",
                "rules" => "required"
            ],
            [
                "label" => "Judul Kegiatan",
                "field" => "judul",
                "rules" => "required"
            ],
            [
                "label" => "Tanggal Kegiatan",
                "field" => "tanggal",
                "rules" => "required"
            ],
            [
                "label" => "Tempat",
                "field" => "tempat",
                "rules" => "required"
            ],
            [
                "label" => "Peserta",
                "field" => "peserta",
                "rules" => "required"
            ],
            [
                "label" => "Materi Kegiatan",
                "field" => "materi",
                "rules" => "required"
            ],
            [
                "label" => "Mengetahui",
                "field" => "nama_kap",
                "rules" => "required"
            ],
            [
                "label" => "Mengetahui",
                "field" => "nipy_kap",
                "rules" => "required"
            ],
            [
                "label" => "Pembuat Laporan",
                "field" => "nama[]",
                "rules" => "required"
            ],
            [
                "label" => "Pembuat Laporan",
                "field" => "nipy[]",
                "rules" => "required"
            ],
            [
                "label" => "Tanggal Laporan",
                "field" => "lokasi",
                "rules" => "required"
            ],
            [
                "label" => "Tanggal Laporan",
                "field" => "tgl_laporan",
                "rules" => "required"
            ]
        ];
    }

    public function imageUpload()
    {
        $this->load->library('upload');
        $config['upload_path']          = 'assets/img/gbr';
        $config['allowed_types']        = 'jpg|png|jpeg';
        $config['file_name']            = rand() . '.png';
        $config['max_size']             = 10240;
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('lampFoto')) {
            $error = array('error' => $this->upload->display_errors());

            return $error;
        }
        return $this->upload->data();
    }

    public function pdfUpload()
    {
        $this->load->library('upload');
        $config['upload_path']          = 'assets/pdf';
        $config['allowed_types']        = 'pdf';
        $config['file_name']            = rand() . '.pdf';
        $config['max_size']             = 10240;
        $this->upload->initialize($config);

        if (!$this->upload->do_upload('lampPdf')) {
            $error = array('error' => $this->upload->display_errors());

            return $error;
        }
        return $this->upload->data();
    }

    // private function _upload()
    // {
    //     $this->load->library('upload');
    //     $data = [];
    //     $config['upload_path']          = 'assets/img/gbr';
    //     $config['allowed_types']        = 'jpg|png|jpeg';
    //     $config['file_name']            = rand() . '.png';
    //     $config['max_size']             = 10240;
    //     $gambar = $_FILES;
    //     $fileCount = count($_FILES['gambar']['name']);
    //     for ($i = 0; $i < $fileCount; $i++) {

    //         $_FILES['gambar']['name'] = $gambar['gambar']['name'][$i];
    //         $_FILES['gambar']['type'] = $gambar['gambar']['type'][$i];
    //         $_FILES['gambar']['tmp_name'] = $gambar['gambar']['tmp_name'][$i];
    //         $_FILES['gambar']['error'] = $gambar['gambar']['error'][$i];
    //         $_FILES['gambar']['size'] = $gambar['gambar']['size'][$i];

    //         $this->upload->initialize($config);

    //         if (!$this->upload->do_upload('gambar')) {
    //             $error = array('error' => $this->upload->display_errors());

    //             return $error;
    //         }

    //         $data[] = $this->upload->data();
    //     }
    //     return $data;
    // }

    // private function _upload2()
    // {
    //     $this->load->library('upload');
    //     $data = [];
    //     $config['upload_path']          = 'assets/pdf/';
    //     $config['allowed_types']        = 'pdf';
    //     $config['file_name']            = rand() . '.pdf';
    //     $this->upload->initialize($config);

    //     if (!$this->upload->do_upload('pdf')) {
    //         $error = array('error' => $this->upload->display_errors());

    //         return $error;
    //     }

    //     return $this->upload->data();
    // }

    public function getAllLaporan()
    {
        $this->db->select('*');
        $this->db->from($this->_tbDDoc);
        $this->db->join($this->_tbDoc, "{$this->_tbDDoc}.id_dok = {$this->_tbDoc}.id_dok");
        $res = $this->db->get();
        if ($res->num_rows() == false) {
            return false;
            die();
        }
        return $res->result();
    }

    public function getLaporanPage($limit, $start)
    {
        $this->db->select('*');
        $this->db->from($this->_tbDDoc);
        $this->db->join($this->_tbDoc, "{$this->_tbDDoc}.id_dok = {$this->_tbDoc}.id_dok");
        $this->db->limit($limit);
        $this->db->offset($start);
        $res = $this->db->get();
        if ($res->num_rows() == false) {
            return false;
            die();
        }
        return $res->result();
    }

    public function getTotalLaporan()
    {
        return $this->db->get('dokumen')->num_rows();
    }

    public function getDokById($id = null)
    {
        $res = $this->db->get_where($this->_tbDoc, ['id_dok' => $id])->row();
        return $res;
    }

    public function getLampiranById($id = null)
    {
        $this->db->select('id_lampiran,lampiran.id_dok,jenis,caption,file');
        $this->db->from($this->_tbDoc);
        $this->db->join('lampiran', "{$this->_tbDoc}.id_dok = lampiran.id_dok");
        $this->db->where("{$this->_tbDoc}.id_dok", $id);
        $res = $this->db->get();
        if ($res->num_rows() == false) {
            return $files = [
                'files' => null
            ];
            die();
        }
        $files['files'] = $res->result();
        return $files;
    }

    public function getLaporanById($id = null)
    {
        $lap = [];
        $this->db->select('*');
        $this->db->from($this->_tbDDoc);
        $this->db->join($this->_tbDoc, "{$this->_tbDoc}.id_dok = {$this->_tbDDoc}.id_dok");
        $this->db->where("{$this->_tbDoc}.id_dok", $id);
        $res = $this->db->get();
        $lamp = $this->getLampiranById($id);
        if ($res->num_rows() == false) {
            return false;
            die();
        }
        $result = (object) array_merge($lap, (array) $res->row(), (array) $lamp);
        return $result;
    }

    public function insertLaporan($dosen = null)
    {
        $this->nama_lap = $this->input->post('nama_lap');
        $this->judul =  $this->input->post('judul');
        $this->tanggal = $this->input->post('tanggal');
        $this->tempat = $this->input->post('tempat');
        $this->pembicara = implode('?', $this->input->post('pembicara'));
        $this->fasilitas = implode('?', $this->input->post('fasilitas'));
        $this->peserta = $this->input->post('peserta');
        $this->materi = $this->input->post('materi');
        $this->tmp_tgl = ucfirst($this->input->post('lokasi')) . ', ' . $this->input->post('tgl_laporan');
        $nama_penyusun = $this->input->post('nama');
        $nipy_penyusun = $this->input->post('nipy');
        if (count($nama_penyusun) != count($nipy_penyusun)) {
            return false;
        }
        for ($i = 0; $i < count($nama_penyusun); $i++) {
            $nama = implode('&', $nama_penyusun);
            $nipy = implode('|', $nipy_penyusun);
        }
        $this->pembuat = $nama . "~" . $nipy;
        $this->kaprodi = implode('?', [$this->input->post('nama_kap'), $this->input->post('nipy_kap')]);
        $this->file_pdf = 'doc_' . rand() . '.pdf';
        $gbr = $this->input->post('caption');
        $pdef = $this->input->post('pdf');
        if (isset($gbr) && isset($pdef)) {
            foreach ($gbr as $key => $value) {
                $img[] = [
                    'jenis' => 'gambar',
                    'file' => (string) $key . '.png',
                    'nama' => $value
                ];
            }
            foreach ($pdef as $key => $value) {
                $pdf[] = [
                    'jenis' => 'pdf',
                    'file' => (string) $key . '.pdf',
                    'nama' => $value
                ];
            }
        } elseif (isset($gbr) || isset($pdef)) {
            if (isset($gbr)) {
                foreach ($gbr as $key => $value) {
                    $img[] = [
                        'jenis' => 'gambar',
                        'file' => (string) $key . '.png',
                        'nama' => $value
                    ];
                }
                $pdf = [];
            } else {
                foreach ($pdef as $key => $value) {
                    $pdf[] = [
                        'jenis' => 'pdf',
                        'file' => (string) $key . '.pdf',
                        'nama' => $value
                    ];
                }
                $img = [];
            }
        } else {
            $img = null;
            $pdf = null;
        }

        $data = [
            'nama_dok' => $this->input->post('dok') . '_APPLA_' . rand()
        ];
        $this->db->insert($this->_tbDoc, $data);
        $this->id_dok = $this->db->insert_id();
        if ($img == null && $pdf == null) {
            $this->db->insert($this->_tbDDoc, $this);
        } else {
            $arr = [];
            $files = array_merge($arr, $img, $pdf);
            foreach ($files as $file) {
                $this->db->query("INSERT INTO 
                lampiran VALUES('',
                                '{$this->id_dok}',
                                '{$file['jenis']}',
                                '{$file['nama']}',
                                '{$file['file']}'
                                )");
            }
            $this->db->insert($this->_tbDDoc, $this);
        }

        return $this->id_dok;
    }

    public function hpsGbr($id = null)
    {
        if ($id != null) {
            $this->db->delete('lampiran', ['id_lampiran' => $id]);
            if ($this->db->affected_rows() != 1) {
                return false;
            }
        }

        return true;
    }

    public function hpsPdf($id = null)
    {
        if ($id != null) {
            $this->db->delete('lampiran', ['id_lampiran' => $id]);
            if ($this->db->affected_rows() != 1) {
                return false;
            }
        }

        return true;
    }

    public function _updateLampiran($data = null, $id = null)
    {
        if ($data != null || $id != null) {
            if (isset($data['lampiran_gambar']) || isset($data['lampiran_pdf'])) {
                $this->db->update($this->_tbDDoc, $data, ['id_dok' => $id]);
                return $this->db->affected_rows();
            }
        }
    }

    public function updateLaporan()
    {
        $namaGbr = [];
        $this->nama_lap = $this->input->post('nama_lap');
        $this->id_dok = decrypt_url($this->input->post('id_dok'));
        $this->judul =  $this->input->post('judul');
        $this->tanggal = $this->input->post('tanggal');
        $this->tempat = $this->input->post('tempat');
        $this->pembicara = implode('?', $this->input->post('pembicara'));
        $this->fasilitas = implode('?', $this->input->post('fasilitas'));
        $this->peserta = $this->input->post('peserta');
        $this->materi = $this->input->post('materi');
        $this->tmp_tgl = ucfirst($this->input->post('lokasi')) . ', ' . $this->input->post('tgl_laporan');
        $nama_penyusun = $this->input->post('nama');
        $nipy_penyusun = $this->input->post('nipy');
        if (count($nama_penyusun) != count($nipy_penyusun)) {
            return false;
        }
        for ($i = 0; $i < count($nama_penyusun); $i++) {
            $nama = implode('&', $nama_penyusun);
            $nipy = implode('|', $nipy_penyusun);
        }
        $this->pembuat = $nama . "~" . $nipy;
        $this->kaprodi = implode('?', [$this->input->post('nama_kap'), $this->input->post('nipy_kap')]);
        $nm = $this->input->post('dok');
        $res = $this->getDokById($this->id_dok);
        $nm2 = explode('_', $res->nama_dok);
        $data = ['nama_dok' => $nm . '_' . $nm2[1] . '_' . $nm2[2]];
        $this->file_pdf = base64_decode($this->input->post('file_pdf'));
        $gbr = $this->input->post('caption');
        $pdef = $this->input->post('pdf');
        $adaLampiran = $this->input->post('ada');

        if (isset($adaLampiran)) {
            foreach ($adaLampiran as $key => $value) {
                $ada[] = [
                    'id_lampiran' => decrypt_url($key),
                    'caption' => $value
                ];
            }
        } else {
            $ada = null;
        }

        if (isset($gbr) && isset($pdef)) {
            foreach ($gbr as $key => $value) {
                $img[] = [
                    'jenis' => 'gambar',
                    'file' => (string) $key . '.png',
                    'nama' => $value
                ];
            }
            foreach ($pdef as $key => $value) {
                $pdf[] = [
                    'jenis' => 'pdf',
                    'file' => (string) $key . '.pdf',
                    'nama' => $value
                ];
            }
        } elseif (isset($gbr) || isset($pdef)) {
            if (isset($gbr)) {
                foreach ($gbr as $key => $value) {
                    $img[] = [
                        'jenis' => 'gambar',
                        'file' => (string) $key . '.png',
                        'nama' => $value
                    ];
                }
                $pdf = [];
            } else {
                foreach ($pdef as $key => $value) {
                    $pdf[] = [
                        'jenis' => 'pdf',
                        'file' => (string) $key . '.pdf',
                        'nama' => $value
                    ];
                }
                $img = [];
            }
        } else {
            $img = null;
            $pdf = null;
        }

        $this->db->update($this->_tbDoc, $data, ['id_dok' => $this->id_dok]);

        if ($ada != null) {
            foreach ($ada as $value) {
                $this->db->update('lampiran', $value, ['id_lampiran' => $value['id_lampiran']]);
            }
        }

        if ($img == null && $pdf == null) {
            $this->db->update($this->_tbDDoc, $this, ['id_dok' => $this->id_dok]);
        } else {
            $arr = [];
            $files = array_merge($arr, $img, $pdf);
            foreach ($files as $file) {
                $this->db->query("INSERT INTO 
                lampiran VALUES('',
                                '{$this->id_dok}',
                                '{$file['jenis']}',
                                '{$file['nama']}',
                                '{$file['file']}'
                                )");
            }
            $this->db->update($this->_tbDDoc, $this, ['id_dok' => $this->id_dok]);
        }

        return $this->id_dok;
    }

    public function deleteLaporan($id = null)
    {
        $res = $this->getLaporanById($id);
        $this->db->delete($this->_tbDoc, ['id_dok' => $id]);
        if ($this->db->affected_rows() == 1) {
            $this->db->delete('lampiran', ['id_dok' => $id]);
            return $this->deleteFiles($res->file_pdf, $res->files);
            die;
        }
        return false;
    }

    public function deleteFiles($file_pdf, $files)
    {
        if (!is_null($files)) {
            foreach ($files as $value) {
                if ($value->jenis == 'gambar') {
                    if (unlink('./assets/img/gbr/' . $value->file) == false) {
                        return false;
                    }
                } else if ($value->jenis == 'pdf') {
                    if (unlink('./assets/pdf/' . $value->file) == false) {
                        return false;
                        die;
                    };
                }
            }
        }
        if (unlink('./assets/fresh_pdf/' . $file_pdf)) {
            return true;
            die;
        }
        return false;
    }

    public function cetak($data = null)
    {
        $nm_file = $data->file_pdf;
        if (isset($data->files)) {
            foreach ($data->files as $value) {
                if ($value->jenis == 'gambar' && $value->jenis == 'pdf') {
                    $nm_pdf[] = $value->file;
                    $files = 'gabung';
                } elseif ($value->jenis == 'pdf' || $value->jenis == 'gambar') {
                    if ($value->jenis == 'pdf') {
                        $nm_pdf[] = $value->file;
                        $files = 'gabung';
                    } else {
                        $files = null;
                    }
                } else {
                    $files = null;
                }
            }
        } else {
            $files = null;
        }
        $isi['data'] = $data;
        try {
            ob_start();
            $this->load->view('cetak/overview', $isi);
            $html = ob_get_contents();
            require_once APPPATH . 'third_party/html2pdf.class.php';
            $pdf = new HTML2PDF('P', 'a4', 'en', true, 'UTF-8');
            $pdf->setTestTdInOnePage(false);
            $pdf->setDefaultFont('times', 12);
            $pdf->writeHTML($html);
            ob_end_clean();
            if ($files == 'gabung') {
                $pdf->Output('assets/fresh_pdf/' . $nm_file, 'F');
                foreach ($nm_pdf as $key => $value) {
                    $pdf_file = $this->gabung(['assets/fresh_pdf/' . $nm_file, 'assets/pdf/' . $value], $nm_file);
                }
                if ($pdf_file != false) {
                    return $pdf_file;
                }
            } else if ($files == null) {
                $pdf->Output('assets/fresh_pdf/' . $nm_file, 'F');
                return $nm_file;
            }
            return false;
        } catch (Html2PdfException $e) {
            $pdf->clean();

            // $formatter = new ExceptionFormatter($e);
            // echo $formatter->getHtmlMessage();
            return false;
        }
    }

    public function gabung($files, $nm_file = null)
    {
        require_once('vendor/setasign/fpdf/fpdf.php');
        require_once('vendor/setasign/fpdi/src/autoload.php');
        $fpdi = new Fpdi;
        try {
            foreach ($files as $file) {
                $pageCount = $fpdi->setSourceFile($file);
                for ($i = 1; $i <= $pageCount; $i++) {
                    $pageId = $fpdi->importPage($i);
                    $s = $fpdi->getTemplateSize($pageId);
                    $fpdi->AddPage($s['orientation'], $s);
                    $fpdi->useImportedPage($pageId);
                }
            }
            $fpdi->Output('assets/fresh_pdf/' . $nm_file, 'F');
            return $nm_file;
        } catch (Html2PdfException $a) {
            return false;
        }
    }
}


// if ($_FILES['gambar']['error'][0] == 0 && $_FILES['pdf']['error'] == 0) {
//     $upload2 = $this->_upload2();
//     $upload = $this->_upload();
//     if (isset($upload2['error']) || isset($upload['error'])) {
//         return false;
//         die;
//     }
//     foreach ($upload as $value) {
//         $namaGbr[] = $value['file_name'];
//     }
//     $this->lampiran_gambar = implode('?', $namaGbr);
//     $this->lampiran_pdf = $upload2['file_name'];
// } else if ($_FILES['pdf']['error'] == 0) {
//     $upload2 = $this->_upload2();
//     if (isset($upload2['error'])) {
//         $this->session->set_flashdata('error', $upload2['error']);
//         redirect('form_laporan');
//         die;
//     }
//     $this->lampiran_pdf = $upload2['file_name'];
// } else if ($_FILES['gambar']['error'][0] == 0) {
//     $upload = $this->_upload();
//     if (isset($upload['error'])) {
//         $this->session->set_flashdata('error', $upload['error']);
//         redirect('form_laporan');
//         die;
//     }
//     foreach ($upload as $value) {
//         $namaGbr[] = $value['file_name'];
//     }

//     $this->lampiran_gambar = implode('?', $namaGbr);
// }
