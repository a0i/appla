<?php

class Dosen_model extends CI_model
{
    private $_tblDos = 'dosen';
    private $_tblJbtn = 'jabatan';
    private $_tblLvl = 'level';

    public function getAll()
    {
        $this->db->select('*');
        $this->db->from($this->_tblDos);
        $this->db->join($this->_tblJbtn, "{$this->_tblDos}.jabatan_fungsional = {$this->_tblJbtn}.id_jab");
        $this->db->join($this->_tblLvl, "{$this->_tblDos}.level = {$this->_tblLvl}.id_level");
        $res = $this->db->get();
        if ($res->num_rows() == false) {
            return false;
            die();
        }
        return $res->result();
    }

    public function getById($id = null)
    {
        $this->db->select('*');
        $this->db->from($this->_tblDos);
        $this->db->join($this->_tblJbtn, "{$this->_tblDos}.jabatan_fungsional = {$this->_tblJbtn}.id_jab");
        $this->db->join($this->_tblLvl, "{$this->_tblDos}.level = {$this->_tblLvl}.id_level");
        $this->db->where('id_dos', $id);
        $res = $this->db->get();
        if ($res->num_rows() == false) {
            return false;
            die();
        }
        return $res->row();
    }
}
