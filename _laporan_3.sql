-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for _laporan_3
DROP DATABASE IF EXISTS `_laporan_3`;
CREATE DATABASE IF NOT EXISTS `_laporan_3` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `_laporan_3`;

-- Dumping structure for table _laporan_3.detail_dok
DROP TABLE IF EXISTS `detail_dok`;
CREATE TABLE IF NOT EXISTS `detail_dok` (
  `id_dok` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `nama_lap` text NOT NULL,
  `tanggal` varchar(30) NOT NULL,
  `tempat` varchar(255) NOT NULL,
  `pembicara` text NOT NULL,
  `fasilitas` text NOT NULL,
  `peserta` text NOT NULL,
  `materi` varchar(255) NOT NULL,
  `tmp_tgl` varchar(50) NOT NULL,
  `kaprodi` varchar(255) NOT NULL,
  `pembuat` varchar(255) NOT NULL,
  `file_pdf` varchar(255) NOT NULL,
  KEY `det_dok` (`id_dok`),
  CONSTRAINT `det_dok` FOREIGN KEY (`id_dok`) REFERENCES `dokumen` (`id_dok`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table _laporan_3.dokumen
DROP TABLE IF EXISTS `dokumen`;
CREATE TABLE IF NOT EXISTS `dokumen` (
  `id_dok` int(11) NOT NULL AUTO_INCREMENT,
  `nama_dok` varchar(50) NOT NULL,
  `dibuat` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `diubah` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_dok`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

-- Dumping structure for table _laporan_3.lampiran
DROP TABLE IF EXISTS `lampiran`;
CREATE TABLE IF NOT EXISTS `lampiran` (
  `id_lampiran` int(11) NOT NULL AUTO_INCREMENT,
  `id_dok` int(11) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `caption` text NOT NULL,
  `file` varchar(255) NOT NULL,
  PRIMARY KEY (`id_lampiran`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
